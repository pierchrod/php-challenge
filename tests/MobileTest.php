<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Interfaces\CarrierInterface;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = $this->createMock(CarrierInterface::class);
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_return_call_when_contact_exist()
	{
		$provider = $this->createMock(CarrierInterface::class);
		$mobile = new Mobile($provider);
		$call = $mobile->makeCallByName("Pierre");
		$this->assertInstanceOf("\\App\\Call", $call);
	}

	/** @test */
	public function it_return_null_when_contact_not_exist()
	{
		$provider = $this->createMock(CarrierInterface::class);
		$mobile = new Mobile($provider);
		$this->assertNull($mobile->makeCallByName("Angelo"));
	}

	/** @test */
	public function it_send_sms_when_number_is_valid()
	{
		$provider = $this->createMock(CarrierInterface::class);
		$mobile = new Mobile($provider);
		$sms = $mobile->sendSMStoNumber("+51999888777", "My Message");

		$this->assertInstanceOf("\\App\\Sms", $sms);
	}

}
