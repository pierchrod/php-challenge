<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

class ContactServiceTest extends TestCase
{
	
	/** @test */
	public function it_return_contact_when_find_by_name()
	{
		$contactService = ContactService::findByName("Pierre");
		$this->assertInstanceOf("\\App\\Contact", $contactService);
	}

	/** @test */
	public function it_return_null_when_find_by_name()
	{
		$contactService = ContactService::findByName("Angelo");
		$this->assertNull($contactService);
	}
	
	/** @test */
	public function it_return_true_when_is_valid_phone_number()
	{
		$contactService = ContactService::validateNumber("+51999888777");
		$this->assertTrue($contactService);
	}

	/** @test */
	public function it_return_false_when_is_invalid_phone_number()
	{
		$contactService = ContactService::validateNumber("++51999888777");
		$this->assertFalse($contactService);
	}
}
