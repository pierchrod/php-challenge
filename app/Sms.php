<?php

namespace App;
use Twilio\Rest\Client;

class Sms
{
	
	const TWILIO_SID = "AC58a82efaaf7e5773e2e9e74a17c4c369";
	const TWILIO_TOKEN = "53ed6b03369df1eed5a36f68515f4d62";
	const TWILIO_NUMBER = "+16283332045";

	function __construct()
	{
		# code...
	}

	function send($phone, $body, $callbackURL = ""){
		$client = new Client(self::TWILIO_SID, self::TWILIO_TOKEN);
		return $client->messages->create(
			$phone, [
				"from" => self::TWILIO_NUMBER,
				"body" => $body,
				"statusCallback" => $callbackURL
			]);
	}
}