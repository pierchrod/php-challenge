<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName($name): ?Contact
	{
		$user = new Contact();
		if($name == "Pierre"){
			$user->setName("Pierre");
			$user->setNumber(999888777);
			return $user;
		}
	
		return null;
	}

	public static function validateNumber(string $number): bool
	{
		return preg_match('/^[+][0-9]+/', $number);
	}
}