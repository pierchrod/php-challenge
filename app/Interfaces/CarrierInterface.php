<?php

namespace App\Interfaces;

use App\Contact;
use App\Call;
use App\Sms;

interface CarrierInterface
{
	//BONUS - Support for two mobile carriers
	public function setCarrier($carrier);

	public function dialContact(Contact $contact);

	public function makeCall(): Call;

	public function sendSMS($number, $body):Sms;
}