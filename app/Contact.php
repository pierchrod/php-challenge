<?php

namespace App;


class Contact
{
	
	public $name;
	public $number;
	
	function __construct()
	{
		
	}

	function getName(){
		return $this->name;
	}

	function setName($name){
		$this->name = $name;
	}

	function getNumber(){
		return $this->number;
	}

	function setNumber($number){
		$this->number = $number;
	}
}