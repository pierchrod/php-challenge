<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		if(!isset($contact->number)) return null;

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSMStoNumber($number, $body)
	{
		if( ContactService::validateNumber($number) && !empty($body))
			return $this->provider->sendSMS($number, $body);
		return false;
	}

	public function sendSMStoNumberTwilio($number, $body)
	{
		if( ContactService::validateNumber($number) && !empty($body)){
			$sms = $this->provider->sendSMS($number, $body);
			$sms->send($number, $body);
			return $sms;
		}
		return false;
	}
}
